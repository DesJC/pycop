#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import tkinter
import glob
import chess.pgn

import handleMove


def searchForGamesWithMoves(moveStack):
    foundGames = []

    for filepath in glob.iglob("./game_lib/**/*.pgn", recursive=True):
        pgn = open(filepath, encoding="ascii")
        # TODO : fix reading pgn twice
        foundHeaders = chess.pgn.read_game(
            pgn, Visitor=chess.pgn.HeadersBuilder)
        foundBoard = chess.pgn.read_game(pgn, Visitor=chess.pgn.BoardBuilder)
        foundMoves = foundBoard.move_stack[0:len(moveStack)]
        if foundMoves == moveStack:
            foundGames.append((foundHeaders, foundBoard))

    return foundGames


def searchGames(layout, engine):
    layout['gamesList'].games = searchForGamesWithMoves(
        layout['boardCanvas'].board.move_stack)

    layout['gamesList'].delete(0, tkinter.END)
    for key, game in enumerate(layout['gamesList'].games):
        header, board = game
        layout['gamesList'].insert(key, "%s vs %s, [%s]" % (
            header["White"], header["Black"], header["Date"]))

    layout['gamesList'].bind(
        '<Double-1>', lambda event: applySelectedGame(layout, engine))


def applySelectedGame(layout, engine):
    index = layout['gamesList'].curselection()[0]
    handleMove.setBoard(layout['gamesList'].games[index][1], layout, engine)

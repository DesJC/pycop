#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Mapping
from configparser import ConfigParser

CONFIG_FILE_LOCATION = "./config.ini"


def getConfig() -> Mapping[str, str]:
    """ Gets the configuration map

    Reads the 'DEFAULT' section of the configuration file indicated by CONFIG_FILE_NAME

    Typical usage:
        config = getConfig()
        value = config['key']
    Returns:
        A map-like object describing the configuration file
        see https://docs.python.org/3/library/configparser.html#mapping-protocol-access
    """

    config = ConfigParser()
    config.read(CONFIG_FILE_LOCATION)
    return config["DEFAULT"]


CONFIG_OBJECT = getConfig()

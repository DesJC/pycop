#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import tkinter
from boardCanvas import BoardCanvas


def dummyCallback(text):
    def eventHandler():
        print(text)
    return eventHandler


def getWindow():
    window = tkinter.Tk()
    window.title("PyCOP")
    window.iconphoto(False, tkinter.PhotoImage(file="./res/P.png"))

    return window


def getMenu(parent):
    menu = tkinter.Menu(parent)
    menu.add_command(label="Exit", command=parent.destroy)

    parent.config(menu=menu)

    return menu


def getRoot(parent):
    root = tkinter.Frame(parent)
    root.pack(side=tkinter.TOP, padx=15, pady=15)

    return root


def getLeftFrame(parent):
    leftFrame = tkinter.Frame(parent)
    leftFrame.pack(side=tkinter.LEFT, fill=tkinter.BOTH)

    return leftFrame


def getEvaluationLabel(parent):
    evaluationLabel = tkinter.Label(parent, text="Eval")
    evaluationLabel.pack(side=tkinter.TOP, fill=tkinter.X)

    return evaluationLabel


def getBoardCanvas(parent):
    boardCanvas = BoardCanvas(parent, squareSize=85)
    boardCanvas.pack(side=tkinter.TOP, padx=15, pady=15)

    return boardCanvas


def getMoveButtonsFrame(parent):
    moveButtonsFrame = tkinter.Frame(parent)
    moveButtonsFrame.pack(side=tkinter.TOP, fill=tkinter.X)

    return moveButtonsFrame


def getBackButton(parent):
    backButton = tkinter.Button(
        parent, text="Back", command=dummyCallback("back"))
    backButton.pack(side=tkinter.LEFT, padx=100)

    return backButton


def getForwardButton(parent):
    forwardButton = tkinter.Button(
        parent, text="Forward", command=dummyCallback("forward"))
    forwardButton.pack(side=tkinter.RIGHT, padx=100)

    return forwardButton


def getRightFrame(parent):
    rightFrame = tkinter.Frame(parent)
    rightFrame.pack(side=tkinter.RIGHT, fill=tkinter.BOTH)

    return rightFrame


def getResetButton(parent):
    resetButton = tkinter.Button(
        parent, text="Reset", command=dummyCallback("reset"))
    resetButton.pack(side=tkinter.TOP, pady=25)

    return resetButton


def getBestMoveLabel(parent):
    bestMoveLabel = tkinter.Label(parent, text="Best move is : Qb5")
    bestMoveLabel.pack(side=tkinter.TOP, pady=25)

    return bestMoveLabel


def getSearchButton(parent):
    searchButton = tkinter.Button(
        parent, text="Search", command=dummyCallback("search"))
    searchButton.pack(side=tkinter.TOP, pady=25)

    return searchButton


def getGamesList(parent):
    frame = tkinter.Frame(parent)
    frame.pack(side=tkinter.TOP, pady=25)

    scrollbar = tkinter.Scrollbar(frame)
    scrollbar.pack(side=tkinter.RIGHT, fill=tkinter.Y)

    gameList = tkinter.Listbox(
        frame, yscrollcommand=scrollbar.set, width=50, height=20)
    gameList.pack(side=tkinter.LEFT)

    scrollbar.config(command=gameList.yview)

    return gameList


def getWidgetsMap():
    widgetsMap = {}

    # level0 - window
    window = widgetsMap['window'] = getWindow()
    widgetsMap['menu'] = getMenu(window)
    # level1 - root
    root = widgetsMap['root'] = getRoot(window)
    # level2 - left
    leftFrame = widgetsMap['leftFrame'] = getLeftFrame(root)
    widgetsMap['evaluationLabel'] = getEvaluationLabel(leftFrame)
    widgetsMap['boardCanvas'] = getBoardCanvas(leftFrame)
    # level3 - move buttons
    moveButtonsFrame = widgetsMap['moveButtonsFrame'] = getMoveButtonsFrame(
        leftFrame)
    widgetsMap['backButton'] = getBackButton(moveButtonsFrame)
    widgetsMap['forwardButton'] = getForwardButton(moveButtonsFrame)
    # level2 - right
    rightFrame = widgetsMap['rightFrame'] = getRightFrame(root)
    widgetsMap['resetButton'] = getResetButton(rightFrame)
    widgetsMap['bestMoveLabel'] = getBestMoveLabel(rightFrame)
    widgetsMap['searchButton'] = getSearchButton(rightFrame)
    widgetsMap['gamesList'] = getGamesList(rightFrame)

    return widgetsMap

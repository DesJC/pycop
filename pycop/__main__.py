#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import tkinter
import chess
import stockfish

import evaluation
import handleMove
import moveStack
import saveAndLoad
import gameSearch
import layout
import configuration


def main():
    layoutMap = layout.getWidgetsMap()
    engine = stockfish.Stockfish(
        "./engine/stockfish_13_linux_x64/stockfish_13_linux_x64")

    # get engine's evaluation of current position
    evaluation.updateEvaluationLabel(layoutMap, engine)
    # get engine's evaluation on next best move
    evaluation.updateBestMoveLabel(layoutMap, engine)

    layoutMap['menu'].add_command(label="Save", command=lambda: saveAndLoad.saveCurrentGame(layoutMap))
    layoutMap['menu'].add_command(label="Load", command=lambda: saveAndLoad.loadGame(layoutMap, engine))

    layoutMap['boardCanvas'].canvas.bind(
        "<Button-1>", handleMove.getBoardCanvasClickHandler(layoutMap, engine))

    layoutMap['resetButton'].config(
        command=lambda: handleMove.setBoard(chess.Board(), layoutMap, engine))

    layoutMap['backButton'].config(
        command=lambda: moveStack.showPreviousMove(layoutMap, engine))
    layoutMap['forwardButton'].config(
        command=lambda: moveStack.showNextMove(layoutMap, engine))

    layoutMap['searchButton'].config(
        command=lambda: gameSearch.searchGames(layoutMap, engine))

    layoutMap['root'].mainloop()

if __name__ == "__main__":
    main()

#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import tkinter
import pickle
import handleMove
from tkinter.filedialog import asksaveasfile
from tkinter.filedialog import askopenfilename

# https://stackoverflow.com/a/19476284/7743022


def saveCurrentGame(layout):
    file = asksaveasfile(mode='wb', defaultextension=".bin")
    if file is None:  # asksaveasfile return `None` if dialog closed with "cancel".
        return
    pickle.dump(layout["boardCanvas"].board, file)
    file.close()


def loadGame(layout, engine):
    filename = askopenfilename()
    if filename is None:
        return
    file = open(filename, mode="rb")
    handleMove.setBoard(pickle.load(file), layout, engine)
    file.close()

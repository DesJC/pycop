#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import tkinter
import chess
import stockfish

import evaluation
import moveStack


def getBoardCanvasClickHandler(layout, engine):
    canvas = layout['boardCanvas']

    def handleBoardCanvasClick(event):
        board = layout['boardCanvas'].board
        if board.is_game_over():
            return

        from_square = canvas.selectedSquare
        canvas.selectSquare(event.x, event.y)
        to_square = canvas.selectedSquare

        if from_square is not None and to_square is not None:
            move = buildMove(from_square, to_square, board)

            if move in board.legal_moves:
                # when making a new move, the stack of move needs to be cleared
                moveStack.clearMoveStack(layout)
                makeMove(move, layout, engine)

            canvas.unselectSquare()

    return handleBoardCanvasClick


def buildMove(from_square, to_square, board):
    promotion = None

    # changes promotion piece to queen when pawn reaches the end of the board
    if board.piece_at(from_square) is not None:
        is_pawn = board.piece_at(from_square).piece_type == chess.PAWN
        is_at_the_end = chess.square_rank(
            to_square) == 0 or chess.square_rank(to_square) == 7
        if is_pawn and is_at_the_end:
            promotion = chess.QUEEN

    return chess.Move(from_square, to_square, promotion)


def makeMove(move, layout, engine):
    board = layout['boardCanvas'].board
    # make the move on the actual board
    board.push(move)

    engine.set_fen_position(board.fen())
    # get engine's evaluation of current position
    evaluation.updateEvaluationLabel(layout, engine)
    # get engine's evaluation on next best move
    evaluation.updateBestMoveLabel(layout, engine)

    # update the forward and back button
    moveStack.updateForwardButton(layout)
    moveStack.updateBackButton(layout)

    # show the move on canvas
    layout['boardCanvas'].updateFromMove(move)
    if board.is_game_over():
        layout['boardCanvas'].showCheckmate()


def setBoard(board, layout, engine):
    # update the actual board
    layout['boardCanvas'].board = board

    engine.set_fen_position(board.fen())
    # get engine's evaluation of current position
    evaluation.updateEvaluationLabel(layout, engine)
    # get engine's evaluation on next best move
    evaluation.updateBestMoveLabel(layout, engine)

    # update the forward and back button
    moveStack.updateForwardButton(layout)
    moveStack.updateBackButton(layout)

    # show the updated board on canvas
    layout['boardCanvas'].draw()
    if board.is_game_over():
        layout['boardCanvas'].showCheckmate()

#!/usr/bin/python3

# MIT License
#
# Copyright (c) 2021 Jean-Claude Desrosiers
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import chess
import tkinter
import math

from PIL import ImageTk, Image

MARGIN_COLOR = '#cd853f'
GRAYED_MARGIN_COLOR = '#101010'
DARK_SQUARE_COLOR = '#88debb'
LIGHT_SQUARE_COLOR = '#feffed'
SELECTED_DARK_SQUARE_COLOR = '#68be9b'
SELECTED_LIGHT_SQUARE_COLOR = '#dedfcd'


class BoardCanvas(tkinter.Frame):
    def __init__(self, parent, squareSize=100, marginSize=20, board=chess.Board()):
        tkinter.Frame.__init__(self, parent)
        self.parent = parent

        self.board = board

        # initialized in resize() because they depend on the size
        self.canvas = None
        self.squareSize = None
        self.checkerSize = None
        self.totalSize = None
        self.origin = None

        self.pieceImageFileMap = {}
        for piece_type in chess.PIECE_TYPES:
            for color in chess.COLORS:
                piece_symbol = chess.Piece(piece_type, color).symbol()
                self.pieceImageFileMap[piece_symbol] = Image.open(
                    './res/%s.png' % piece_symbol)
        # the following map is initialized in resize() because it takes into account square length
        # initializing it here would mean it's done twice on object creation
        self.pieceIconMap = {}

        self.selectedSquare = None

        self.resize(squareSize, marginSize)

    def resize(self, squareSize=100, marginSize=20):
        self.squareSize = squareSize
        self.checkerSize = 8 * squareSize
        self.totalSize = 2 * marginSize + self.checkerSize

        self.canvas = tkinter.Canvas(
            self, bg=MARGIN_COLOR, width=self.totalSize, height=self.totalSize)

        # top-left corner of the checker
        self.origin = (marginSize, marginSize)

        for piece_symbol, image_file in self.pieceImageFileMap.items():
            self.pieceIconMap[piece_symbol] = ImageTk.PhotoImage(
                image_file.resize((squareSize, squareSize), Image.ANTIALIAS))

        self.draw()

    # gets top-left coordinate of a square
    def _getCoordinate(self, square):
        file, rank = (chess.square_file(square), chess.square_rank(square))

        # first square (a1) is in the bottom-left corner, explains the (7-rank)
        return (self.origin[0] + self.squareSize * file, self.origin[1] + self.squareSize * (7-rank))

    # gets square from coordinate in it (relative to top-left corner of canvas)
    def _getSquare(self, x, y):
        # checks for out-of-bound click
        if x < self.origin[0] or \
                x > self.origin[0] + self.checkerSize or \
            y < self.origin[1] or \
                y > self.origin[1] + self.checkerSize:
            return None

        file, rank = (math.floor((x - self.origin[0]) / self.squareSize),
                      7-math.floor((y - self.origin[1]) / self.squareSize))

        return chess.square(file, rank)

    def _drawChecker(self, affected_squares):
        for square in affected_squares:
            file, rank = (chess.square_file(square), chess.square_rank(square))
            color = [DARK_SQUARE_COLOR,
                     LIGHT_SQUARE_COLOR][(file+rank) % 2]
            selected_color = [SELECTED_DARK_SQUARE_COLOR,
                              SELECTED_LIGHT_SQUARE_COLOR][(file+rank) % 2]

            # first square (a1) is in the bottom-left corner
            x1, y1 = self._getCoordinate(square)
            x2 = x1 + self.squareSize
            y2 = y1 + self.squareSize

            if square == self.selectedSquare:
                self.canvas.create_rectangle(
                    x1, y1, x2, y2, outline=selected_color, fill=selected_color)
            else:
                self.canvas.create_rectangle(
                    x1, y1, x2, y2, outline=color, fill=color)

    def _drawPieces(self, affected_squares):
        for square, piece in self.board.piece_map().items():
            # only update a square if it has a piece on it AND it's affected
            if square in affected_squares:
                x, y = self._getCoordinate(square)
                icon = self.pieceIconMap[piece.symbol()]

                # add half square length because icon is positioned at its center
                self.canvas.create_image(
                    x + self.squareSize / 2, y + self.squareSize / 2, image=icon)

    def draw(self, affected_squares=chess.SQUARES):
        self._drawChecker(affected_squares)
        self._drawPieces(affected_squares)
        self.canvas.pack(side=tkinter.TOP)

    # only calls draw for affected squares by the move, does not apply move to internal board
    def updateFromMove(self, move):
        # the only moves which affect squares
        # other than their from/to squares are castling and en passant
        if self.board.is_castling(move) or self.board.is_en_passant(move):
            self.draw()
        else:
            self.draw([move.from_square, move.to_square])

    # x and y should be relative to the top-left corner of the canvas
    def selectSquare(self, x, y):
        clickedSquare = self._getSquare(x, y)

        if clickedSquare in chess.SQUARES:
            if clickedSquare == self.selectedSquare:
                self.unselectSquare()
            else:
                affected_squares = [clickedSquare]
                # otherwise affected square is None on first click
                if self.selectedSquare is not None:
                    affected_squares.append(self.selectedSquare)

                self.selectedSquare = clickedSquare
                self.draw(affected_squares)

    def unselectSquare(self):
        # need it so we know what square to erase selection
        previously_selected = self.selectedSquare
        self.selectedSquare = None
        self.draw([previously_selected])

    def showCheckmate(self):
        self.canvas = tkinter.Canvas(
            self, bg=GRAYED_MARGIN_COLOR, width=self.totalSize, height=self.totalSize)
        self.draw()

    def unshowCheckmate(self):
        self.canvas = tkinter.Canvas(
            self, bg=MARGIN_COLOR, width=self.totalSize, height=self.totalSize)
        self.draw()
